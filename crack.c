#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
      char *g = md5(guess, strlen(guess));

    // Compare the two hashes
    
    
    if(strcmp(g, hash) == 0 ) 
    {
        return 1;
    }
        return 0;

    
    
    
     

    // Free any malloc'd memory
    free(guess);
   
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
     struct stat st;
    if (stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    
    char *file = malloc(len);
    
    
 
 // read entire file into memory
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("cant open %s for read\n", filename);
        exit(1);
        
    }
    
    fread(file,1,len,f);
 
 // replace \n with \0
    int count = 0;
    for(int i = 0; i < len; i++)
    {
        if(file[i] == '\n')
        {
           file[i] = '\0';
           count++;
        }
    }
 
 // malloc space for array of pointers
    char **line = malloc((count+1) * sizeof(char *));
 
 // fill in addresses
    int word = 0;
    line[word] = file; // the first word in the file
    word++;
    for(int i = 0; i < len; i++)
    {
        if(file[i] == '\0' && i+1 < len)
        {
            line[word] = &file[i+1];
            word++;
        }
    }
    
    line[word] = NULL;
    
 // return address of second array
    //*size = word-1;
    return line;
}



int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile("hashes.txt");

    // Read the dictionary file into an array of strings
    char **dict = readfile("rockyou100.txt");

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
   
  
    for(int i = 0; hashes[i] != NULL; i++)
    {
     
        for(int j = 0; dict[j] != NULL; j++)
        {
            if(tryguess(hashes[i], dict[j]) == 1) 
            {
             printf("hash is : %s password is %s\n",hashes[i],dict[j]);
               
            }
        }
    }
}
